#pragma onece

#include <string>
#include <boost/shared_ptr.hpp>
#include "nodelet/nodelet.h"
#include <ros/ros.h>
#include "ros_cuda_stereo/CudaStereoConfig.h"
#include <image_geometry/pinhole_camera_model.h>
#include <image_geometry/stereo_camera_model.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>
#include <camera_calibration_parsers/parse.h>
#include <dynamic_reconfigure/server.h>
#include <opencv2/cudastereo.hpp>
#include <opencv2/cudawarping.hpp>


namespace ros_cuda_stereo
{

using message_filters::sync_policies::ExactTime;

class StereoCudaProcessor
{
    using SubscriberFilter_t = image_transport::SubscriberFilter;
    using InfoSubscriber_t   = message_filters::Subscriber<sensor_msgs::CameraInfo>;
    using ImageSubscriber_t  = message_filters::Subscriber<sensor_msgs::Image>;
    using ImageExactPolicy_t = ExactTime<sensor_msgs::Image, sensor_msgs::Image>;
    using InfoExactPolicy_t  = ExactTime<sensor_msgs::CameraInfo, sensor_msgs::CameraInfo>;
    using ImageExactSync_t   = message_filters::Synchronizer<ImageExactPolicy_t>;
    using InfoExactSync_t    = message_filters::Synchronizer<InfoExactPolicy_t>;

  public:
    StereoCudaProcessor(ros::NodeHandle& nh, ros::NodeHandle& privateNh);
    virtual void init();

  private:

    ros::NodeHandle nh_, private_nh_;
    CudaStereoConfig oldConfig_;
    CudaStereoConfig config_;

  protected:
    bool configReady_ = false;
    boost::shared_ptr<image_transport::ImageTransport> imageTransport_;
    boost::shared_ptr<InfoSubscriber_t> info_left_sub_, info_right_sub_;
    boost::shared_ptr<InfoExactSync_t> info_exact_sync_;

    boost::shared_ptr<ImageSubscriber_t> image_left_sub_, image_right_sub_;
    boost::shared_ptr<ImageExactSync_t> image_exact_sync_;

    std::string camNameLeft;
    std::string camNameRight;
    boost::shared_ptr<dynamic_reconfigure::Server<CudaStereoConfig> > drServer_;

    // camera models
    image_geometry::PinholeCameraModel leftModel_;
    image_geometry::PinholeCameraModel rightModel_;
    image_geometry::StereoCameraModel stereoModel_;

    ros::Publisher image_left_rect_pub_;
    ros::Publisher image_right_rect_pub_;

    cv::cuda::GpuMat image_cuda_left_raw, image_cuda_right_raw;
    cv::cuda::GpuMat image_cuda_left_rect, image_cuda_right_rect;

    void DynamicReconfigureCb(CudaStereoConfig &config, uint32_t level);

    void StartCamerInfo();
    void StopCamerInfo();
    void StartImages();
    void StopImages();
    void ReloadCameraModels();
    std::string getCameraConfigLeftFile();
    std::string getCameraConfigRightFile();
    std::string getCameraTopicLeftInfo();
    std::string getCameraTopicRightInfo();
    std::string getCameraTopicRightImage();
    std::string getCameraTopicLeftImage();
    void CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr &l_info_msg, const sensor_msgs::CameraInfoConstPtr &r_info_msg);
    void CameraImageCallback(const sensor_msgs::ImageConstPtr &l_image_msg, const sensor_msgs::ImageConstPtr &r_image_msg);
    std::string getCameraTopicLeftRectImage();
    std::string getCameraTopicRightRectImage();
    void updatePublishers();
}; // class StereoCudaNodelet

} // namespace ros_cuda_stereo

