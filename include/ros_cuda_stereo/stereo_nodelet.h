#pragma once

#include <nodelet/nodelet.h>
#include "ros_cuda_stereo/cuda_stereo_processor.h"

namespace ros_cuda_stereo
{

class StereoCudaNodelet: public nodelet::Nodelet
{
public:
  StereoCudaNodelet()
  {}

  ~StereoCudaNodelet()
  {

  }

private:
  virtual void onInit();
  boost::shared_ptr<ros_cuda_stereo::StereoCudaProcessor> processor;
};
} // namespace
