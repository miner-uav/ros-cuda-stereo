
#include "ros_cuda_stereo/cuda_stereo_processor.h"
#include "pluginlib/class_list_macros.h"
#include "std_msgs/String.h"
#include <boost/timer.hpp>

#include <cv_bridge/cv_bridge.h>

namespace ros_cuda_stereo
{

StereoCudaProcessor::StereoCudaProcessor(ros::NodeHandle &nh, ros::NodeHandle &privateNh)
    : nh_(nh)
    , private_nh_(privateNh)
{
    leftModel_.setRectificationMapType(CV_32F);
    rightModel_.setRectificationMapType(CV_32F);
}

void StereoCudaProcessor::init()
{

    info_left_sub_ = boost::make_shared<InfoSubscriber_t>();
    info_right_sub_ = boost::make_shared<InfoSubscriber_t>();
    image_left_sub_ = boost::make_shared<ImageSubscriber_t>();
    image_right_sub_ = boost::make_shared<ImageSubscriber_t>();
    imageTransport_ = boost::make_shared<image_transport::ImageTransport>(private_nh_);

    info_exact_sync_ = boost::make_shared<InfoExactSync_t>(InfoExactPolicy_t(10u), *info_left_sub_, *info_right_sub_);
    info_exact_sync_->registerCallback(boost::bind(&StereoCudaProcessor::CameraInfoCallback, this, _1, _2));

    image_exact_sync_ = boost::make_shared<ImageExactSync_t>(ImageExactPolicy_t(10u), *image_left_sub_, *image_right_sub_);
    image_exact_sync_->registerCallback(boost::bind(&StereoCudaProcessor::CameraImageCallback, this, _1, _2));

    // Dynamic Reconfigure server:
    drServer_ = boost::make_shared<dynamic_reconfigure::Server<CudaStereoConfig> >(private_nh_);
    drServer_->setCallback(boost::bind(&StereoCudaProcessor::DynamicReconfigureCb, this, _1, _2));

    image_left_rect_pub_ = private_nh_.advertise<sensor_msgs::Image>("left/image_rect", 10);
    image_right_rect_pub_ = private_nh_.advertise<sensor_msgs::Image>("right/image_rect", 10);

    ROS_INFO("StereoCudaNodelet loaded");
}

void StereoCudaProcessor::DynamicReconfigureCb(CudaStereoConfig &config, uint32_t level)
{
    ROS_INFO("DynamicReconfigureCb");
    oldConfig_ = config_;
    config_ = config;
    if ((level & CudaStereo_LEVEL_CAMERA_MODEL) == CudaStereo_LEVEL_CAMERA_MODEL)
    {
        ReloadCameraModels();
    }
}

void StereoCudaProcessor::CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr &l_info_msg, const sensor_msgs::CameraInfoConstPtr &r_info_msg)
{
    leftModel_.fromCameraInfo(l_info_msg);
    rightModel_.fromCameraInfo(r_info_msg);
    stereoModel_.fromCameraInfo(l_info_msg, r_info_msg);
    configReady_ = true;

    // we do not need to listen to this anymore
    StopCamerInfo();
    StartImages();

    ROS_INFO_STREAM("Stereo calibration initialized from topics; left:" << camNameLeft << "; right:" << camNameRight << "; baseline:" << fabs(stereoModel_.baseline() * 100.0)
                                                                        << " [cm];");
}

void StereoCudaProcessor::CameraImageCallback(const sensor_msgs::ImageConstPtr &l_image_msg, const sensor_msgs::ImageConstPtr &r_image_msg)
{
    if (image_left_rect_pub_.getNumSubscribers() > 0)
    {
        boost::timer perf_timer;
        // get the cv::Mat
        cv::Mat temp_mat = cv_bridge::toCvShare(l_image_msg, l_image_msg->encoding)->image;
        perf_timer.restart();
        // move the image to GPU memory
        image_cuda_left_raw.upload(temp_mat);
        //adjust the output size if needed
        if (image_cuda_left_raw.size() != image_cuda_left_rect.size())
        {
            image_cuda_left_rect.create(image_cuda_left_raw.rows, image_cuda_left_raw.cols, image_cuda_left_raw.type());
        }
        //do the job - rectify
        leftModel_.rectifyImage(image_cuda_left_raw, image_cuda_left_rect);

        //download the image from GPU
        image_cuda_left_rect.download(temp_mat);
        double dur = perf_timer.elapsed();
        //create message
        sensor_msgs::ImageConstPtr left_rect_msg = (cv_bridge::CvImage(l_image_msg->header, l_image_msg->encoding, temp_mat).toImageMsg());
        //publish it
        image_left_rect_pub_.publish(left_rect_msg);
        ROS_INFO("Rectification took: %.2f [ms]", dur * 1000.0);
    }
}

std::string StereoCudaProcessor::getCameraConfigLeftFile() { return config_.cam_config_folder + "/" + config_.cam_left_calib_file; }

std::string StereoCudaProcessor::getCameraConfigRightFile() { return config_.cam_config_folder + "/" + config_.cam_right_calib_file; }

std::string StereoCudaProcessor::getCameraTopicLeftInfo() { return config_.cam_left_topic + "/" + config_.cam_left_info_suffix; }

std::string StereoCudaProcessor::getCameraTopicRightInfo() { return config_.cam_right_topic + "/" + config_.cam_right_info_suffix; }

std::string StereoCudaProcessor::getCameraTopicLeftImage() { return config_.cam_left_topic + "/" + config_.cam_left_image_raw_suffix; }

std::string StereoCudaProcessor::getCameraTopicRightImage() { return config_.cam_right_topic + "/" + config_.cam_right_image_raw_suffix; }

std::string StereoCudaProcessor::getCameraTopicLeftRectImage() { return config_.cam_left_topic + "/" + config_.cam_left_image_rect_suffix; }

std::string StereoCudaProcessor::getCameraTopicRightRectImage() { return config_.cam_right_topic + "/" + config_.cam_right_image_rect_suffix; }

void StereoCudaProcessor::ReloadCameraModels()
{
    configReady_ = false;
    if (config_.cam_config_source == CudaStereo_CameraConfigSourceTopics)
    {
        StartCamerInfo();
        camNameLeft = config_.cam_left_topic;
        camNameRight = config_.cam_right_topic;
    }
    else
    {
        sensor_msgs::CameraInfo left, right;
        std::string left_file = getCameraConfigLeftFile();
        std::string right_file = getCameraConfigRightFile();

        ROS_INFO_STREAM("Loading LEFT Camera Configuration; File:" << left_file);
        camera_calibration_parsers::readCalibration(left_file, camNameLeft, left);

        ROS_INFO_STREAM("Loading RIGHT Camera Configuration; File:" << right_file);
        camera_calibration_parsers::readCalibration(right_file, camNameRight, right);

        leftModel_.fromCameraInfo(left);
        rightModel_.fromCameraInfo(right);
        stereoModel_.fromCameraInfo(left, right);
        configReady_ = true;
        StartImages();
        ROS_INFO_STREAM("Stereo calibration initialized from files; left:" << camNameLeft << "; right:" << camNameRight << "; baseline:" << fabs(stereoModel_.baseline() * 100.0)
                                                                           << " [cm];");
    }
}

void StereoCudaProcessor::StartCamerInfo()
{
    std::string left_topic = getCameraTopicLeftInfo();
    std::string right_topic = getCameraTopicRightInfo();

    ROS_INFO_STREAM("Subscribing to Camera Info; topic:" << left_topic);
    info_left_sub_->subscribe(private_nh_, left_topic, 1);

    ROS_INFO_STREAM("Subscribing to  Camera Info; topic:" << right_topic);
    info_right_sub_->subscribe(private_nh_, right_topic, 1);
}

void StereoCudaProcessor::StopCamerInfo()
{
    ROS_INFO("Unsubscribing from camera infos");
    info_left_sub_->unsubscribe();
    info_right_sub_->unsubscribe();
}

void StereoCudaProcessor::StartImages()
{
    std::string left_topic = getCameraTopicLeftImage();
    std::string right_topic = getCameraTopicRightImage();

    ROS_INFO_STREAM("Subscribing to Cemra images; topic:" << left_topic);
    image_left_sub_->subscribe(private_nh_, left_topic, 1);

    ROS_INFO_STREAM("Subscribing to Cemra images; topic:" << right_topic);
    image_right_sub_->subscribe(private_nh_, right_topic, 1);
}

void StereoCudaProcessor::StopImages()
{
    ROS_INFO("Unsubscribing from camera images");
    image_left_sub_->unsubscribe();
    image_right_sub_->unsubscribe();
}

} // namespace ros_cuda_stereo
