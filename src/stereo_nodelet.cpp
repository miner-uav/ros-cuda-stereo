
#include "ros_cuda_stereo/stereo_nodelet.h"
#include "pluginlib/class_list_macros.h"
#include "std_msgs/String.h"

#include <cv_bridge/cv_bridge.h>

namespace ros_cuda_stereo
{

void StereoCudaNodelet::onInit()
{
    auto privatehn = getPrivateNodeHandle();
    auto nh = getNodeHandle();
    processor = boost::make_shared<StereoCudaProcessor>(nh, privatehn);
    processor->init();
}

} // namespace ros_cuda_stereo
PLUGINLIB_EXPORT_CLASS(ros_cuda_stereo::StereoCudaNodelet, nodelet::Nodelet);

