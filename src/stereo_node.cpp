
#include "ros_cuda_stereo/stereo_nodelet.h"
#include <ros/ros.h>

int main(int argc, char **argv)
{
    ROS_INFO("main");
    ros::init(argc, argv, "ros_cuda_stereo");
    ros::NodeHandle global_node;
    ros::NodeHandle priv_nh("~");

    ROS_INFO("Creating processor");
    ros_cuda_stereo::StereoCudaProcessor processor(global_node, priv_nh);
    ROS_INFO("initialising");
    processor.init();
    ros::spin();
    return 0;
}
