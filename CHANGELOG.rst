^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package ros_cuda_stereo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.1 (2018-03-22)
------------------
* Initial development of nodelet
* Contributors: Maciej Matuszak
